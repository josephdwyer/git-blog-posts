A Git Blog  
==  
##Gitpress?  
###Why?  
I have pondered working on a blog off and on for years. On one such
occasion, I thought I could make a blog running entirely off of git.
This rudamentary blog is a proof of concept.  

###How?  
Both [GitHub] and [BitBucket] offer the abilty to host static websites, they
also both have easy to use APIs. Simple enough, just use
[GitHub]/[BitBucket] to host a static site that uses javascript to hit their
api and download the posts. Add few simple bells and whistles like a
[ShowDown] javascript markdown renderer and [CodeMirror] for displaying code blocks
and that is what I call a ball.

###Workflow  
1. Write a blog post using markdown syntax.  
```markdown
  A Git Blog  
  ==  
  ##Gitpress?  
  ###Why?  
  I have pondered working on a blog off and on for years. On one such
```

1. Also, make a small json file containing some metadata.
```
    { "title": "A Git Blog",
      "subtitle": "Gitpress?",
      "date": "2013 10 30"
    }
```

1. Check new post into a repository containing blog posts.
```
    git add 2013_10_30_first.md
    git commit -m 'adding first post'
    git push origin master
```

1. Done - the blog post now is on the webpage.

All with no server side code!

###What's left?
1. Any styling at all.
1. [Github] support.
1. A commenting system.. preferably with no database
  * DisQus - or a similar service.
  * Use Wiki or Issues System?
  * A repository containing comments with public commit access?
  * A database?

[GitHub]:http://www.github.com
[BitBucket]:http://www.bitbucket.org
[CodeMirror]:http://codemirror.net/
[ShowDown]:https://github.com/coreyti/showdown
